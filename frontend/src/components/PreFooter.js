import {Container, Row, Col, Form, Button} from 'react-bootstrap';
import Logo from "../images/gdlogofooter.png";

export default function PreFooter() {
    return (
        <footer className="pre-footer py-3 mt-1">
            <Row>
                <Col xs={6} md={4}>
                    <h5 className="pre-footer-header">ABOUT</h5>
                    <div className="pre-footer-content">
                        <img className="pre-footer-icon" src={Logo}/>
                        <p className="pre-footer-p1"><strong>Gold Diggers <sup className="pre-footer-p11">PH</sup></strong></p>
                    </div>
                    <div className="pre-footer-content2">
                         <p>We aim to be the undisputed leader in preloved jewelry retail. Backed by a profitable and trustworthy household brand.</p>
                    </div>
                </Col>

                <Col xs={6} md={2}> 
                    <h5 className="pre-footer-header">MENU</h5>
                    <div className="pre-footer-content">
                        <p className="pre-footer-p2"><strong>&#x2022; Home</strong></p>
                        <p className="pre-footer-p2"><strong>&#x2022; Products</strong></p>
                        <p className="pre-footer-p2"><strong>&#x2022; My Account</strong></p>
                        <p className="pre-footer-p2"><strong>&#x2022; My Cart</strong></p>
                    </div>
                </Col>

                <Col xs={6} md={3}>
                    <h5 className="pre-footer-header">CONTACT US</h5>
                    <div>
                        <p className="contact-us-p">New Lower Bicutan, Taguig City</p>
                    </div>

                    <div>
                        <p className="contact-us-p">+02831389</p>
                    </div>

                    <div>
                        <p className="contact-us-p">golddiggersph@mail.com</p>
                    </div>


                </Col>

                <Col xs={6} md={3}>
                    <h5 className="pre-footer-header">NEWSLETTER</h5>
                    <p className="pre-footer-p4">If you are new to Gold Diggers PH, we're glad you are here. <strong>Stay informed of Our Special Offers</strong></p>
                    
                    <Form>
                        <div className="row"> 
                          <Form.Group className="newsletter-form mb-3 col-md-8" controlId="formBasicEmail">
                            <Form.Control type="email" placeholder="email@mail.com" />
                          </Form.Group>

                          <Button className="newsletter-button mb-3 mr-5 col-md-4"variant="dark" type="submit">
                                Subscribe
                          </Button>
                        </div>
                    </Form>
                </Col>
            </Row>
        </footer>
    )
}
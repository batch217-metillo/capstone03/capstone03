import {Button, Container, Form, Nav, Navbar, NavDropdown, Dropdown} from 'react-bootstrap'
import SearchIcon from '@mui/icons-material/Search';
import Image from "../images/gdlogo.png";
import {Link, NavLink} from 'react-router-dom'
import {useContext} from 'react'
import UserContext from '../UserContext.js'

export default function AppNavbar() {

  // const [user, setUser] = useState(localStorage.getItem("emailAddress"));
  // console.log(user)

  const {user} = useContext(UserContext);
  console.log(user);

  return (

    <Navbar className="navbar" expand="lg" sticky="top">
      <Container className="d-flex" fluid>
        <img 
            src={Image}
            width="40"
            height="40"
            className="me-2"
            alt="logo"
          />
        <Navbar.Brand className="navbar-brand-logo mx-3" style={{color: "#d4af37" }} as={Link} to="/">Gold Diggers <sup className="navbar-brand-ph">PH</sup></Navbar.Brand>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav">

        {/*  <Form className="search-bar d-flex mx-5 justify-content-center">
              <Form.Control
                type="search"
                placeholder="Search"
                className="me-2"
                aria-label="Search"
              />
              <button type="submit" className='btn-search'><SearchIcon className='search-icon' style={{ fontSize: "2em", color: "#FFD700" }} /></button>
          </Form>*/}

          <Nav className="navbar-names ms-auto">

              {
                  (user.isAdmin)
                  ?
                  <>
                  <Nav.Link className="mx-1" style={{color: "#FFFFFF" }} as={NavLink} to="/">Home</Nav.Link>
                  <Nav.Link className="mx-1" style={{color: "#FFFFFF" }} as={NavLink} to="/admin">Admin Dashboard</Nav.Link>
                  </>
                  :
              <>
              <Nav.Link className="mx-1" style={{color: "#FFFFFF" }} as={NavLink} to="/">Home</Nav.Link>
              <Nav.Link className="mx-1" style={{color: "#FFFFFF" }} as={NavLink} to="/products">Products</Nav.Link>
              </>
              }

              {
                (user.id !== null)
                ?
                <Dropdown>
                      <Dropdown.Toggle className="appNavDropdown" id="dropdown-basic">
                        User Profile
                      </Dropdown.Toggle>

                      <Dropdown.Menu>
                        <Dropdown.Item as={NavLink} to="/profile">My Account</Dropdown.Item>
                        <Dropdown.Item as={NavLink} to="/cart">My Cart</Dropdown.Item>
                        <Dropdown.Item as={NavLink} to="/logout">Logout</Dropdown.Item>
                        <Dropdown.Divider />
                        <Dropdown.Item as={NavLink} to="/help">Help</Dropdown.Item>
                      </Dropdown.Menu>
                </Dropdown>
                :
                <>
                  <Nav.Link className="mx-1" style={{color: "#FFFFFF" }} as={NavLink} to="/login">Login</Nav.Link>
                  <Nav.Link className="mx-1" style={{color: "#FFFFFF" }} as={NavLink} to="/register">Register</Nav.Link>
                 
                </>
              }
          </Nav>
        </Navbar.Collapse>
      </Container>
    </Navbar>
  );
}
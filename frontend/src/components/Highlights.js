import {Container, Row, Col, Button} from 'react-bootstrap';
import Card from 'react-bootstrap/Card';
import CardGroup from 'react-bootstrap/CardGroup';
import {Link} from "react-router-dom";
import Icon1 from "../images/FSICON.png";
import Icon2 from "../images/CODICON.png";
import Icon3 from "../images/UVICON.png";
import Icon4 from "../images/EHICON.png";

export default function Highlights({highlightsProp}) {

  const {title, description, destination, label} = highlightsProp;

  return (
    <>
      <Row>
        <Col className="py-2 mb-1 text-center font-link">
                <h1 className="highlights-title">{title}</h1>
                <p className="highlights-description mb-2">{description}</p>

                <Container className="p-1 text-center">
                    <Button className="buttonA text-center" as = {Link} to={destination} variant="secondary">{label}</Button>
                </Container>
        </Col>
      </Row>

      <Row>
        <Col>
          <CardGroup className="mt-3">
           <Card>
              <Card.Body>
                <div className="content">
                    <img className="icon" src={Icon1}/>
                    <p className="icon-header"><strong>FREE SHIPPING</strong></p>
                    <p className="icon-header-p">This is a wider card.</p>
                </div>
              </Card.Body>
            </Card>
            <Card>
              <Card.Body>
                <div className="content">
                    <img className="icon" src={Icon2}/>
                    <p className="icon-header"><strong>CASH ON DELIVERY</strong></p>
                    <p className="icon-header-p">This is a wider card.</p>
                </div>
              </Card.Body>
            </Card>
            <Card>
              <Card.Body>
                <div className="content">
                    <img className="icon" src={Icon3}/>
                    <p className="icon-header"><strong>UNLI VOUCHERS</strong></p>
                    <p className="icon-header-p">This is a wider card.</p>
                </div>
              </Card.Body>
            </Card>
            <Card>
              <Card.Body>
                <div className="content">
                    <img className="icon" src={Icon4}/>
                    <p className="icon-header"><strong>EXCELLENT QUALITY</strong></p>
                    <p className="icon-header-p">This is a wider card.</p>
                </div>
              </Card.Body>
            </Card>
          </CardGroup>
        </Col>
      </Row>
    </>
  );
}

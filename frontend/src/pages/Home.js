import Banner from '../components/Banner.js';
import Highlights from '../components/Highlights.js';
import ProductCategoryCard from '../components/ProductCategoryCard.js';

export default function Home() {
	const data = {
		title: "WELCOME, Au DIGGERS!",
		description: "Everything feels different with GOLD.",
		destination: "/products",
		label: "Shop Now!"
	}

	return (
		<>
			<Banner/>
			<Highlights highlightsProp={data}/>
			<ProductCategoryCard/>
		</>
	)
}
import {Carousel, Container, Row, Col, Form, Button} from 'react-bootstrap';
import {useState, useEffect, useContext} from 'react';
import UserContext from '../UserContext.js';
import { useNavigate } from "react-router-dom";
import {Navigate} from 'react-router-dom';
import Swal from "sweetalert2";
import RSlider1 from "../images/Rslider1.png";
import RSlider2 from "../images/Rslider2.png";
import RSlider3 from "../images/Rslider3.png";

export default function Register(){

	const {user} = useContext(UserContext);
	const navigate = useNavigate();

	const [firstName, setFirstName] = useState('');
	const [lastName, setLastName] = useState('');
	const [username, setUsername] = useState('');
	const [emailAddress, setEmailAddress] = useState('');
	const [phoneNumber, setPhoneNumber] = useState('');
	const [password1, setPassword1] = useState('');
	const [password2, setPassword2] = useState('');
	const [isActive, setIsActive] = useState(false);

	console.log(firstName);
	console.log(lastName);
	console.log(username);
	console.log(emailAddress);
	console.log(phoneNumber);
	console.log(password1);
	console.log(password2);

	useEffect(() => {

		if((firstName !== '' && lastName !== '' && username !== '' && emailAddress !== '' && phoneNumber!== '' && password1 !== '' && password2 !== '') && (password1 === password2)) {
			setIsActive(true)
		}else {
			setIsActive(false)
		}
	}, [firstName, lastName, username, emailAddress, phoneNumber, password1, password2])

	function registerUser(e){
	    e.preventDefault();

	    fetch(`${process.env.REACT_APP_ECOMMERCEAPI_URL}/checkEmail`, {
	        method: "POST",
	        headers:{
	            "Content-Type": "application/json"
	        },
	        body: JSON.stringify({
	            emailAddress: emailAddress
	        })
	    })
	    .then(res => res.json())
	    .then(data =>{
	        console.log(data);

	        if(data){
	            Swal.fire({
	                title: "Duplicate email address found",
	                icon: "error",
	                text: "Kindly provide another email to complete the registration."
	            })
	        }else{

	            fetch(`${process.env.REACT_APP_ECOMMERCEAPI_URL}/register`,{
	                method: "POST",
	                headers:{
	                    "Content-Type": "application/json"
	                },
	                body: JSON.stringify({
	                    firstName: firstName,
	                    lastName: lastName,
	                    username: username,
	                    emailAddress: emailAddress,
	                    phoneNumber: phoneNumber,
	                    password: password1,
	                })
	            })
	            .then(res => res.json())
	            .then(data => {
	                console.log(data);

	                if(data){
	                    Swal.fire({
	                        title: "Registration Successful",
	                        icon: "success",
	                        text: "Welcome to Gold Diggers PH!"
	                    });
	                   setFirstName('');
	                   setLastName('');
	                   setUsername('');
	                   setEmailAddress('');
	                   setPhoneNumber('');
	                   setPassword1('');
	                   setPassword2('');
	                   navigate("/login");
	                }
	                else{

	                    Swal.fire({
	                        title: "Something went wrong",
	                        icon: "error",
	                        text: "Please try again."
	                    });
	                }
	            })
	        }
	    })
	}

	return (
		(user.id !== null)
		?
		<Navigate to="/"/>
		:
		<Container fluid>
		      
		      <Row>
		        <Col xs={12} md={6}>
		          <Carousel className="carousel-banner mt-1">
                      {/*FIRST CAROUSEL ITEM*/}
                        <Carousel.Item>
                          <img
                            className="d-block w-100"
                            src={RSlider1}
                            alt="First slide"
                          />
                        </Carousel.Item>

                      {/*SECOND CAROUSEL ITEM*/}
                        <Carousel.Item>
                          <img
                            className="d-block w-100"
                            src={RSlider2}
                            alt="Second slide"
                          />
                        </Carousel.Item>

                      {/*THIRD CAROUSEL ITEM*/}
                        <Carousel.Item>
                          <img
                            className="d-block w-100"
                            src={RSlider3}
                            alt="Third slide"
                          />
                        </Carousel.Item>
                      </Carousel>
		        </Col>

		        <Col className="p-2" xs={12} md={6}>

		        <h4 className="register-title"> Welcome to Gold Diggers PH</h4>
		          <Form onSubmit={(e) => registerUser(e)}>
		          	<div className="row"> 
		          	{/*FIRST NAME*/}
		          		<Form.Group className="form-group col-md-6 mb-1" controlId="firstName">
		                  <Form.Label className="register-item">First Name</Form.Label>
		                  <Form.Control className="register-item" type="text" placeholder="Enter First Name" 
		                  required onChange={e => setFirstName(e.target.value)} value={firstName}/>
		                </Form.Group>

		            {/*LAST NAME*/}
		                <Form.Group className="form-group col-md-6 mb-1" controlId="lastName">
		                  <Form.Label className="register-item">Last Name</Form.Label>
		                  <Form.Control className="register-item" type="text" placeholder="Enter Last Name" 
		                  required onChange={e => setLastName(e.target.value)} value={lastName}/>
		                </Form.Group>
		            </div>

		            {/*USERNAME*/}
		                <Form.Group className="mb-1" controlId="username">
		                  <Form.Label className="register-item">Username</Form.Label>
		                  <Form.Control className="register-item" type="text" placeholder="Enter Username" 
		                  required onChange={e => setUsername(e.target.value)} value={username}/>
		                </Form.Group>

		            {/*EMAIL ADDRESS*/}
		                <Form.Group className="mb-1" controlId="emailAddress">
		                  <Form.Label className="register-item">Email Address</Form.Label>
		                  <Form.Control className="register-item" type="email" placeholder="name@example.com" 
		                  required onChange={e => setEmailAddress(e.target.value)} value={emailAddress}/>
		                </Form.Group>

		            {/*PHONE NUMBER*/}
		                <Form.Group className="mb-1" controlId="phoneNumber">
		                  <Form.Label className="register-item">Phone Number</Form.Label>
		                  <Form.Control className="register-item" type="text" placeholder="+639xxxxxxxxx" 
		                  required onChange={e => setPhoneNumber(e.target.value)} value={phoneNumber}/>
		                </Form.Group>

		            <div className="row">
		            {/*PASSWORD 1*/}
		                <Form.Group className="form-group col-md-6 mb-1" controlId="password1">
		                  <Form.Label className="register-item">Password</Form.Label>
		                  <Form.Control className="register-item" type="password" placeholder="Password" 
		                  required onChange={e => setPassword1(e.target.value)} value={password1}/>
		                </Form.Group>

		            {/*PASSWORD 2*/}
		                <Form.Group className="form-group col-md-6 mb-1" controlId="password2">
		                  <Form.Label className="register-item">Verify Password</Form.Label>
		                  <Form.Control className="register-item" type="password" placeholder="Verify Password" 
		                  required onChange={e => setPassword2(e.target.value)} value={password2}/>
		                </Form.Group>
		                {/*<Form.Group className="mb-3" controlId="formBasicCheckbox">
		                  <Form.Check type="checkbox" label="Check me out" />
		                </Form.Group>*/}
		            </div>

		            {/*SUBMIT BUTTON*/}
	                	{
	                		isActive 
	                		?
	                		<Container className="p-2 text-center">
	                		<Button className="register-button" variant="primary" type="submit" id="submitBtn">
	                		Submit
	                		</Button>
	                		</Container>
	                		:
	                		<Container className="p-2 text-center">
	                		<Button className="register-button" variant="primary" type="submit" id="submitBtn" disabled>
	                		Register
	                		</Button>
	                		</Container>
	                	}
	                 <p className="text-center">Already have an account? <a href="/login">Login here!</a></p>

		                
		          </Form>
		        </Col>
		      </Row>
		</Container>

		
	)
}
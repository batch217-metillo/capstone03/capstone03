import {useEffect, useState, useContext} from "react";
import {Navigate} from 'react-router-dom';
import {Container} from "react-bootstrap";
import ProductCard from "../components/ProductCard.js";
import UserContext from "../UserContext";

export default function Products() {

	const {user} = useContext(UserContext);

	const [products, setProducts] = useState([]); 

	useEffect(() =>{
		
		fetch(`${process.env.REACT_APP_ECOMMERCEAPI_URL}/product/active`)
		.then(res => res.json())
		.then(data => {
			console.log(data);
			
			setProducts(data.map(product =>{
				return(
					<ProductCard key={product._id} productProp={product}/>
				);
			}));
		})
	}, []);
	return(
		(user.isAdmin)
		?
			<Navigate to="/admin" /> 
		:
		<>
			<h1 className="product-header text-center p-4 font-link-bold">Products</h1>
			<Container fluid className="cards pb-5">
			{products}
			</Container>
		</>

	)
}
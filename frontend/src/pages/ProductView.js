import {useState, useEffect, useContext} from "react";

import {Container, Card, Button, Row, Col, Form} from "react-bootstrap";
import {useParams, useNavigate, Link} from "react-router-dom";
import UserContext from "../UserContext.js"
import Swal from "sweetalert2";

export default function ProductView(){

	const {user} = useContext(UserContext);

	const {productId} = useParams();
	const navigate = useNavigate();

	const [name, setName] = useState('');
	const [category, setCategory] = useState('');
	const [description, setDescription] = useState('');
	const [stocks, setStocks] = useState('');
	const [price, setPrice] = useState(0);
	const [quantity, setQuantity] = useState('');
	const [isActive, setIsActive] = useState(false);

	useEffect(()=>{
		if(quantity <= 0) {
			setIsActive(false);
		} else {
			setIsActive(true);
		}
	}, [quantity])


	useEffect(()=>{
		console.log(productId);

		fetch(`${process.env.REACT_APP_ECOMMERCEAPI_URL}/product/${productId}`)
		.then(res => res.json())
		.then(data => {

			console.log(data);

			setName(data.name);
			setCategory(data.category);
			setDescription(data.description);
			setStocks(data.stocks);
			setPrice(data.price);

		});

	}, [productId])

	const addToCart = (productId) =>{

		fetch(`${process.env.REACT_APP_ECOMMERCEAPI_URL}/user/addToCart`, {
			method: "POST",
			headers: {
				"Content-Type": "application/json",
				Authorization: `Bearer ${localStorage.getItem('token') }`
			},
			body: JSON.stringify({
				products: [{
					productId: productId,
					name: name,
					quantity: quantity
					}],
				totalAmount: quantity*price

			})
		})
		.then(res => res.json())
		.then(data => {

			console.log(data);

			if(data){
				Swal.fire({
					title: "Thank you for purchasing!",
					icon: "success",
					text: "Your order has been placed."
				});

				navigate("/products");
			}
			else{
				Swal.fire({
					title: "Something went wrong",
					icon: "error",
					text: "Please try again."
				});
			}

		});

	}

	return(
		<Container className="mt-5 mb-5 ">
			<Row>
				<Col lg={{ span: 6, offset: 3 }}>
					<Card>
						<Card.Body className="text-center">
							<Card.Title className="fs-4 fw-bold mb-3">{name}</Card.Title>

							<Card.Subtitle  className="fs-5">Category:</Card.Subtitle>
							<Card.Text className="fs-5 mb-3">{category}</Card.Text>

							<Card.Subtitle className="fs-5">Description:</Card.Subtitle>
							<Card.Text className="fs-5 mb-3">{description}</Card.Text>

							<Card.Subtitle className="fs-5">Stocks:</Card.Subtitle>
							<Card.Text className="fs-5 mb-3">{stocks}</Card.Text>

							<Card.Subtitle className="fs-5">Price:</Card.Subtitle>
							<Card.Text className="fs-5 mb-3">₱ {price}</Card.Text>

							<Form className="px-5 mb-3">
                            <Form.Label className="fs-5">Quantity:</Form.Label>
                            <Form.Control 
                            	type="number" 
                            	placeholder="Input quantity" 
                            	value={quantity}
                            	min={0}
                            	onChange={e => setQuantity(e.target.value)}
                            	/>
                            </Form>

                            <Container>
                            {
                            	(user.id !== null)
                            	?
                            		isActive
                            		?
                            		<Button className="addtocart-button" size="lg" type="submit" variant="primary" size="md" onClick={() => addToCart(productId)}>Order</Button>
                            		:
                            		<Button className="addtocart-button" size="lg" type="submit" variant="primary" size="md" onClick={() => addToCart(productId)} disabled>Order</Button>
                            	:
                            	<Button className="addtocart-button" size="lg" type="submit" as={Link} to="/login" variant="secondary" size="md">Login to Order</Button>
                            }
                            </Container>
						</Card.Body>		
					</Card>
				</Col>
			</Row>
		</Container>
	)
}